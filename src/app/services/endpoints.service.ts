import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { stack_over_main } from '../postdata';
import {Observable, Subject} from 'rxjs';
import { title } from 'process';

@Injectable({
  providedIn: 'root'
})
export class EndpointsService {

  constructor( private httpClient : HttpClient ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
};

postUrl_all: string = "https://api.stackexchange.com/2.2/search/advanced/";

all_solutions( postdata: stack_over_main ){

  var param  = new HttpParams({
        fromObject: {
          key: postdata.key,
          site: postdata.site,
          filter: postdata.filter,
          order: postdata.order,          
          page: postdata.page,
          pagesize: postdata.pagesize,
          sort: postdata.sort,
          tagged: postdata.tagged,
          accepted: postdata.accepted,
          wiki: postdata.wiki
        }
        });

  return this.httpClient.get(this.postUrl_all, { params : param});  
}

filter_solutions( postdata: stack_over_main ){

  var param  = new HttpParams({
        fromObject: {
          key: postdata.key,
          site: postdata.site,
          filter: postdata.filter,
          order: postdata.order,          
          page: postdata.page,
          pagesize: postdata.pagesize,
          sort: postdata.sort,
          tagged: postdata.tagged,
          accepted: postdata.accepted,
          wiki: postdata.wiki,
          todate: postdata.todate,
          fromdate: postdata.fromdate,
          title: postdata.title
        }
        });

  return this.httpClient.get(this.postUrl_all, { params : param});  
}


}

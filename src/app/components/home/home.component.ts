import { Component, OnInit } from '@angular/core';
import { EndpointsService } from '../../services/endpoints.service';
import { stack_over_main } from '../../postdata';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private _dataservice: EndpointsService) { }

  main_func(order="desc", page="1", pagesize="15", sort="votes", tagged ="", filter="default", accepted= "True"){
    this.stackoverflow_input = new stack_over_main;
    this.stackoverflow_input.order = order;
    this.stackoverflow_input.page = page;
    this.stackoverflow_input.pagesize = pagesize;
    this.stackoverflow_input.sort = sort;
    this.stackoverflow_input.tagged = tagged;
    this.stackoverflow_input.filter = filter;
    this.stackoverflow_input.tagged = tagged;
    this.stackoverflow_input.accepted = accepted;
    sessionStorage.setItem("input", JSON.stringify(this.stackoverflow_input));
    this._dataservice.all_solutions(this.stackoverflow_input).subscribe( res => {
        this.all_solution_results = res;
        this.items = this.all_solution_results.items;
        console.log(this.all_solution_results);
    }, err => {
      window.alert(JSON.stringify(err));
    });
   }

  ngOnInit() {
    this.main_func("asc", "1", "15", "votes", "");
  }

  stackoverflow_input: stack_over_main;
  all_solution_results;
  items;

  imgloc:string = "../assets/images/pp.jpg";
  name:string= "Kundan";

  search_tags:string;

  language_list = [
      "C#",
      "JavaScript",
      "Angular",
      "CSS" ,
      "PHP" ,
      "Python"  ,
      "React"  ,
      "Java"  ,
      "HTML"  ,
      "Android"  ,
      "IOS"  ,
      "ASP.NET"  ,
      "SQL"  ,
      "Linux"  
   ];

   status: string;

   home_page(){
    this.pages = [
      {
        page_no: 1,
        page_position: "first"
      },
      {
        page_no: 2,
        page_position: "second"
      },
      {
        page_no: 3,
        page_position: "third"
      }
    ];
    this.page_status= "first";
   }

   newest( value ){
    this.main_func( value, "1", "15", "votes", "");
    this.status = value;
    this.home_page();
   }

   actively(value){
    this.main_func( "asc", "1", "15", value, "");
    this.status = value;
    this.home_page();
   }

   votes( value ){
    this.main_func( "desc", "1", "15", value, "");
    this.status = value;
    this.home_page();
   }

   unanswered( ){
    this.main_func( "desc", "1", "15", "votes", "", "!--nSE7U574vj");
    this.status = "unanswered";
    this.home_page();
   }

   creation( value ){
    this.main_func( "desc", "1", "15", value, "");
    this.status = value;
    this.home_page();
   }

   relevance( value ){
    this.main_func( "desc", "1", "15", value, "");
    this.status = value;
    this.home_page();
  }

  filter_status: boolean = false;
  fiter_popup(){
    this.filter_status = !(this.filter_status);
  }

  tag_search(lang){
    this.main_func("desc", "1", "15", "votes", lang, "default", "True");
    this.home_page();
    this.status ="";
  }

  order:string = "desc";
  sort:string = "votes";
  submit( date1, date2, title, tag ){
    this.stackoverflow_input = new stack_over_main;
    this.stackoverflow_input.fromdate = date1;
    this.stackoverflow_input.todate = date2;
    this.stackoverflow_input.order = this.order;
    this.stackoverflow_input.page = "1";
    this.stackoverflow_input.pagesize = "15";
    this.stackoverflow_input.sort = this.sort;
    this.stackoverflow_input.tagged = tag;
    this.stackoverflow_input.filter = "default";
    this.stackoverflow_input.accepted = "accepted";
    this.stackoverflow_input.title = title;
    sessionStorage.setItem("input", JSON.stringify(this.stackoverflow_input));

    this._dataservice.filter_solutions(this.stackoverflow_input).subscribe( resp => {
      this.all_solution_results = resp;
      this.items = this.all_solution_results.items;
      // console.log(this.all_solution_results);
      if(this.all_solution_results.has_more == true){
        this.filter_status = false;
      }
    }, err => {
      window.alert(JSON.stringify(err));
    });
    this.home_page();    
  }

  search_title: string;
  search_title_fun(){
    this.submit("", "", this.search_title, "" );
    this.home_page();
    this.status ="";
  }

  search_title_fun_2(){
    this.submit("", "", this.search_tags, "" );
    this.home_page();
    this.status ="";
  }


  page_status:string = "first";
  pages = [
    {
      page_no: 1,
      page_position: "first"
    },
    {
      page_no: 2,
      page_position: "second"
    },
    {
      page_no: 3,
      page_position: "third"
    }
  ];
  page_value: number = 1;

  page_clicked( value, positon ){    
    this.page_status = positon;
    this.page_value = value;

    this.stackoverflow_input =  JSON.parse(sessionStorage.getItem("input"));
    this.stackoverflow_input.page = value;

    this._dataservice.filter_solutions(this.stackoverflow_input).subscribe( resp => {
      this.all_solution_results = resp;
      this.items = this.all_solution_results.items;
      // console.log(this.all_solution_results);
      if(this.all_solution_results.has_more == true){
        this.filter_status = false;
      }
    }, err => {
      window.alert(JSON.stringify(err));
    });

  }

  next_page(){
    if(this.page_status == "first"){
      this.page_value = this.page_value + 1;
      this.page_clicked(this.page_value, "second");
    }
    else if(this.page_status == 'second'){
      this.page_value = this.page_value + 1;
      this.page_clicked(this.page_value, 'third');
    }
    else if( this.page_status == "third"){

      if(this.page_value <=10){

        if(this.page_value % 3 == 0){
          // console.log(this.page_value);
          for(let i = 0; i <=2; i++){
            this.pages[i].page_no = this.page_value + i+1;
          }
        }

        this.page_value = this.page_value + 1;
        this.page_clicked(this.page_value, "first");

      }
      else{
        window.alert("This is last page.");
      }
     
    }
  }

  previous_page(){
    if(this.page_status == "third"){
      this.page_value = this.page_value - 1;
      this.page_clicked(this.page_value, "second");
    }
    else if(this.page_status == 'second'){
      this.page_value = this.page_value - 1;
      this.page_clicked(this.page_value, "first");
    }
    else if( this.page_status == "first"){
      if(this.page_value > 0){

        if(this.page_value % 3 == 1){
          let j = 2;
          for(let i = 0; i <=2; i++){
            this.pages[j-i].page_no = this.page_value - (i+1);
          }
        }

        // console.log(this.page_value %3 )

        this.page_value = this.page_value - 1;
        this.page_clicked(this.page_value, 'third');
      }
      else{
        window.alert("This is first page.");
      }
      
    }
  }

}

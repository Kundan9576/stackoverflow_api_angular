import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-ans-table',
  templateUrl: './ans-table.component.html',
  styleUrls: ['./ans-table.component.css']
})
export class AnsTableComponent implements OnInit {

  @Input() public get_results;

  constructor() { }

  ngOnInit() {
  }

  deafault_img:string = "../assets/images/pp.jpg";
  name:string= "Kundan";

  move_stackoverflow(){
    window.location.href = this.get_results.link;
  }

  edited_date;

  // ngOnChanges(changes: SimpleChanges){    
  //   console.log(this.get_results.last_edit_date) 
  // }

}

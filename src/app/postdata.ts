export class stack_over_main {
    page: string;
    pagesize: string;
    fromdate: string ="";
    todate: string ="";
    order: string;
    min: string;
    max: string;
    sort: string;
    tagged: string;
    key:string = "U4DMV*8nvpm3EOpvf69Rxw((";
    filter:string;
    site: string = "stackoverflow";
    accepted: string;
    closed: string;
    title: string ="";
    views: string;
    wiki:string = "False";
}

